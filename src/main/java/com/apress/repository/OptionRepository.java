package com.apress.repository;

import com.apress.domain.Option;
import org.springframework.data.repository.CrudRepository;

/**
 * @author Carl Lu
 */
public interface OptionRepository extends CrudRepository<Option, Long> {

}
