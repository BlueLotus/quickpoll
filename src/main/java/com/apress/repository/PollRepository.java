package com.apress.repository;

import com.apress.domain.Poll;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

/**
 * @author Carl Lu
 */
public interface PollRepository extends PagingAndSortingRepository<Poll, Long> {

}
