package com.apress.repository;

import com.apress.domain.User;
import org.springframework.data.repository.CrudRepository;

/**
 * @author Carl Lu
 */
public interface UserRepository extends CrudRepository<User, Long> {

    public User findByUsername(String username);

}
