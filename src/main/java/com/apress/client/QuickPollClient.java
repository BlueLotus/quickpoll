package com.apress.client;

import com.apress.domain.Option;
import com.apress.domain.Poll;
import org.apache.tomcat.util.codec.binary.Base64;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.data.domain.PageImpl;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.security.oauth2.client.OAuth2RestTemplate;
import org.springframework.security.oauth2.client.token.grant.password.ResourceOwnerPasswordResourceDetails;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * @author Carl Lu
 */
public class QuickPollClient {

    private static final String QUICK_POLL_URI_V1 = "http://localhost:8080/v1/polls";
    private static final String QUICK_POLL_URI_V2 = "http://localhost:8080/v2/polls";
    private static final String QUICK_POLL_URI_V3 = "http://localhost:8080/v3/polls";
    private RestTemplate restTemplate = new RestTemplate();

    public Poll getPollById(Long pollId) {
        return restTemplate.getForObject(QUICK_POLL_URI_V1 + "/{pollId}", Poll.class, pollId);
    }

    public Poll getPollByIdForOAuth(Long pollId) {
        OAuth2RestTemplate restTemplate = getOAuth2RestTemplate();
        return restTemplate.getForObject(QUICK_POLL_URI_V3 + "/{pollId}", Poll.class, pollId);
    }

    public List<Poll> getAllPolls() {
        ParameterizedTypeReference<List<Poll>> responseType = new ParameterizedTypeReference<List<Poll>>() {};
        ResponseEntity<List<Poll>> responseEntity = restTemplate.exchange(QUICK_POLL_URI_V1, HttpMethod.GET, null, responseType);
        List<Poll> allPolls = responseEntity.getBody();
        return allPolls;
    }

    /**
     * This method will failed because PageImpl doesn’t have a default constructor and Spring’s HTTP message
     * converter would fail with the following exception:
     *
     * Caused by: com.fasterxml.jackson.databind.JsonMappingException:
     * No suitable constructor found for type [simple type, class
     * org.springframework.data.domain.PageImpl<com.apress.domain.Poll>]:
     * can not instantiate from JSON object (missing default constructor or creator,
     * or perhaps need to add/enable type information?)
     *
     * For the solution, please refer to getAllpolls(int page, int size) method below.
     */
    public PageImpl<Poll> getAllPollsV2() {
        ParameterizedTypeReference<PageImpl<Poll>> responseType = new ParameterizedTypeReference<PageImpl<Poll>>() {};
        ResponseEntity<PageImpl<Poll>> responseEntity = restTemplate.exchange(QUICK_POLL_URI_V2, HttpMethod.GET, null, responseType);
        return responseEntity.getBody();
    }

    public PageWrapper<Poll> getAllpolls(int page, int size) {
        ParameterizedTypeReference<PageWrapper<Poll>> responseType = new ParameterizedTypeReference<PageWrapper<Poll>>() {};
        UriComponentsBuilder builder = UriComponentsBuilder
                                        .fromHttpUrl(QUICK_POLL_URI_V2)
                                        .queryParam("page", page)
                                        .queryParam("size", size);
        ResponseEntity<PageWrapper<Poll>> responseEntity = restTemplate.exchange(builder.build().toUri(), HttpMethod.GET, null, responseType);
        return responseEntity.getBody();
    }

    public URI createPoll(Poll poll) {
        return restTemplate.postForLocation(QUICK_POLL_URI_V1, poll);
    }

    public void updatePoll(Poll poll) {
        restTemplate.put(QUICK_POLL_URI_V1 + "/{pollId}", poll, poll.getId());
    }

    public void deletePoll(Long pollId) {
        restTemplate.delete(QUICK_POLL_URI_V1 + "/{pollId}", pollId);
    }

    public void deletePollV3(Long pollId) {
        getOAuth2RestTemplate().exchange(QUICK_POLL_URI_V3 + "/{pollId}", HttpMethod.DELETE, new HttpEntity<Void>(new HttpHeaders()), Void.class, pollId);
    }

    private HttpHeaders getAuthenticationHeader(String username, String password) {
        String credentials = username + ":" + password;
        byte[] base64CredentialData = Base64.encodeBase64(credentials.getBytes());

        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.set("Authorization", "Basic " + new String(base64CredentialData));
        return httpHeaders;
    }

    private OAuth2RestTemplate getOAuth2RestTemplate() {
        ResourceOwnerPasswordResourceDetails resourceDetails = new ResourceOwnerPasswordResourceDetails();
        resourceDetails.setGrantType("password");
        resourceDetails.setAccessTokenUri("http://localhost:8080/oauth/token");
        resourceDetails.setClientId("quickpolliOSClient");
        resourceDetails.setClientSecret("top_secret");

        // Set scopes
        List<String> scopes = new ArrayList<>();
        scopes.add("read");
        scopes.add("write");
        resourceDetails.setScope(scopes);

        // Resource Owner details
        resourceDetails.setUsername("admin");
        resourceDetails.setPassword("admin");

        return new OAuth2RestTemplate(resourceDetails);
    }

    public static void main(String[] args) {
        QuickPollClient client = new QuickPollClient();
        Poll poll = client.getPollById(1L);
        System.out.println(poll);

        List<Poll> polls = client.getAllPolls();
        System.out.println(polls);

        Poll newPoll = new Poll();
        newPoll.setQuestion("What's your favorite yokai?");
        Set<Option> options = new HashSet<>();
        newPoll.setOptions(options);

        Option option1 = new Option();
        option1.setValue("JibaNyan");
        options.add(option1);

        Option option2 = new Option();
        option2.setValue("KomaSan");
        options.add(option2);

        URI pollLocation = client.createPoll(newPoll);
        System.out.println("Newly created poll location: " + pollLocation);

        client.deletePollV3(1L);

        System.out.println(client.getAllpolls(0, 5).getContent());

        // This should failed (404 not found) cuz the poll with id 1 has already removed.
        System.out.println(client.getPollByIdForOAuth(1L));
    }

}
