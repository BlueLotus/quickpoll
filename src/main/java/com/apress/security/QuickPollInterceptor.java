package com.apress.security;

import org.apache.tomcat.util.codec.binary.Base64;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpRequest;
import org.springframework.http.client.ClientHttpRequestExecution;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.stereotype.Component;

import java.io.IOException;

/**
 * @author Carl Lu
 * This interceptor is not in use.
 */
public class QuickPollInterceptor implements ClientHttpRequestInterceptor {
    @Override
    public ClientHttpResponse intercept(HttpRequest httpRequest, byte[] bytes, ClientHttpRequestExecution clientHttpRequestExecution) throws IOException {
        if (httpRequest.getMethod() == HttpMethod.DELETE){
            String credentials = "admin" + ":" + "admin";
            byte[] base64CredentialData = Base64.encodeBase64(credentials.getBytes());
            httpRequest.getHeaders().add("Authorization", "Basic " + new String(base64CredentialData));
        }
        return clientHttpRequestExecution.execute(httpRequest, bytes);
    }
}
