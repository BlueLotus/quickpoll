package com.apress;

import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = QuickPollApplication.class)
@WebAppConfiguration
public class QuickPollApplicationTests {

	@Test
	public void contextLoads() {
	}

	@Test
	public void readData() {
		readPoll();
	}

	public void readPoll() {
		HttpURLConnection connection = null;
		BufferedReader reader = null;
		try {
			URL restAPIUrl = new URL("http://localhost:8080/v1/polls/1");
			connection = (HttpURLConnection) restAPIUrl.openConnection();
			connection.setRequestMethod("GET");

			reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
			StringBuilder jsonData = new StringBuilder();
			String line;
			while ((line = reader.readLine()) != null) {
				jsonData.append(line);
			}
			System.out.println(jsonData.toString());

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			IOUtils.closeQuietly(reader);
			if(connection != null) {
				connection.disconnect();
			}
		}
	}

}
